import React, {FC, useState} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Alert,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {AppDispatch, RootState} from '../../store';
import {setUsername} from '../../store/user/action';

interface WelcomeProps {}

const Welcome: FC<WelcomeProps> = () => {
  const dispatch = useDispatch<AppDispatch>();
  const state = useSelector((state: RootState) => state.user.step);
  const [value, onChangeText] = useState('');

  const submit = () => {
    if (!value) Alert.alert('Please fill name!');
    else dispatch(setUsername(value));
  };

  const isRender = state === 'set-user-name';
  if (!isRender) return null;

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.title}>{}Quiz App</Text>
        <TextInput
          editable
          multiline
          numberOfLines={4}
          maxLength={40}
          placeholder="Name"
          style={styles.input}
          onChangeText={text => onChangeText(text)}
          value={value}
        />
        <TouchableOpacity style={styles.button} onPress={submit}>
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#D0E1FF',
  },
  input: {
    width: 248,
    height: 48,
    backgroundColor: '#ffffff',
    borderRadius: 25,
    paddingHorizontal: 16,
    marginBottom: 16,
    borderColor: '#1E8CFB',
    borderWidth: 2.5,
    fontSize: 26,
  },
  content: {
    alignItems: 'center',
    padding: 16,
    marginTop: 64,
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 8,
    marginBottom: 16,
  },
  title: {
    fontSize: 32,
    fontWeight: 'bold',
    color: '#1E8CFB',
    marginBottom: 8,
  },
  button: {
    backgroundColor: '#1E8CFB',
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 24,
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default Welcome;
