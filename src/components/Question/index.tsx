import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  Modal,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {AppDispatch, RootState} from '../../store';
import {
  addUserScore,
  setCurrentQuestionIndex,
  setQuestions,
} from '../../store/question/action';
import {map} from 'lodash';
import {setLeaderboards} from '../../store/leaderboard/action';
import Swal from 'react-native-swal';
import Icon from 'react-native-vector-icons/Ionicons';
import {addHistory} from '../../store/user/action';

const correctSentences = [
  'Excellent job! You got it right.',
  "Well done! That's the correct answer.",
  'Perfect! You nailed it.',
  'Great work! You answered correctly.',
  "Bravo! That's spot on.",
  'Fantastic! You hit the mark.',
  'Impressive! You got the right answer.',
  "Superb! That's exactly right.",
  'Outstanding! You got it correct.',
  'Brilliant! You answered perfectly.',
];
const incorrectSentences = [
  "That's not correct. The correct answer is",
  'Wrong answer. The right one is',
  'Incorrect. The correct answer is',
  "Nope, that's not it. The correct answer is",
  "Sorry, that's incorrect. The correct answer is",
  'Incorrect answer. The correct one is',
  'Incorrect, the right answer is',
  "That's wrong. The correct answer is",
  'Incorrect, the correct answer is',
  'Not quite right. The correct answer is',
];

function getRandomNumber() {
  return Math.floor(Math.random() * 9) + 1;
}

const Question = () => {
  const dispatch = useDispatch<AppDispatch>();
  const questions = useSelector((state: RootState) => state.question.questions);
  const name = useSelector((state: RootState) => state.user.name);
  const points = useSelector((state: RootState) => state.question.point);
  const state = useSelector((state: RootState) => state.user.step);
  const [disable, setDisable] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [messageModal, setMessageModal] = useState('');
  const [iconModal, setIconModal] = useState('checkmark-circle');
  const [colorModal, setColor] = useState('checkmark-circle');

  const currentQuestionIndex = useSelector(
    (state: RootState) => state.question.current,
  );
  const isRender = state === 'question';

  useEffect(() => {
    dispatch(setQuestions());
  }, []);

  const handleAnswer = (selectedOption: string) => {
    setDisable(preState => !preState);
    let point = 0;
    let message = '';
    let icon = 'checkmark-circle';
    let color = '#FF0000';
    const isCorrect = selectedOption === questions[currentQuestionIndex].answer;
    if (isCorrect) {
      point += 1;
      message = `${correctSentences[getRandomNumber()]}`;
      icon = 'checkmark-circle';
      color = '#4CAF50';
    } else {
      message = `${incorrectSentences[getRandomNumber()]} ${
        questions[currentQuestionIndex].answer
      }`;
      icon = 'close-circle';
      color = '#FF0000';
    }
    setModalVisible(true);
    setDisable(preState => !preState);
    setMessageModal(message);
    setIconModal(icon);
    setColor(color);
    if (currentQuestionIndex < questions.length - 1) {
      dispatch(addUserScore(point));
    } else {
      dispatch(
        setLeaderboards({
          username: name,
          points: point + points,
        }),
      );
      dispatch(addHistory(point + points));
    }
  };
  
  if (!isRender) return null;

  if (questions.length === 0) {
    return <Text>Loading...</Text>;
  }

  const currentQuestion = questions[currentQuestionIndex];
  if (!currentQuestion) {
    return <Text>Loading...</Text>;
  }
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.header}>Quiz Game</Text>
      <View>
        <View style={styles.questionContainer}>
          <Text style={styles.questionText}>
            {currentQuestionIndex + 1}.{currentQuestion.question}
          </Text>
        </View>
        <View style={styles.optionsContainer}>
          {map(currentQuestion.options, (option, index) => (
            <TouchableOpacity
              disabled={disable}
              key={`${option}${index}`}
              style={styles.optionButton}
              onPress={() => handleAnswer(option)}>
              <Text style={styles.optionText}>{option}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
      <Modal
        transparent={true}
        visible={modalVisible}
        animationType="slide"
        onRequestClose={() => setModalVisible(false)}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <View style={styles.iconContainer}>
              <Icon name={iconModal} size={50} color={colorModal} />
            </View>
            <Text style={styles.modalText}>{messageModal}</Text>
            <TouchableOpacity
              style={{...styles.closeButton, backgroundColor: colorModal}}
              onPress={() => setModalVisible(false)}>
              <Text style={styles.closeButtonText}>Next Question!</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 16,
    backgroundColor: '#D0E1FF',
  },
  header: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 16,
    color: '#1E8CFB',
  },
  questionContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 10,
    padding: 20,
    alignItems: 'center',
    marginBottom: 20,
  },
  questionText: {
    fontSize: 18,
    marginVertical: 10,
  },
  optionsContainer: {
    justifyContent: 'space-around',
  },
  optionButton: {
    backgroundColor: '#1E8CFB',
    padding: 16,
    borderRadius: 8,
    marginVertical: 10,
  },
  optionText: {
    color: '#ffffff',
    fontSize: 24,
    textAlign: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    width: '80%',
    padding: 20,
    backgroundColor: '#FFFFFF',
    borderRadius: 10,
    alignItems: 'center',
  },
  iconContainer: {
    marginBottom: 20,
  },
  modalText: {
    fontSize: 20,
    marginVertical: 20,
    textAlign: 'center',
    color: '#000000',
  },
  closeButton: {
    width: '80%',
    padding: 10,
    borderRadius: 25,
    alignItems: 'center',
    backgroundColor: '#4CAF50',
  },
  closeButtonText: {
    fontSize: 18,
    color: '#fff',
  },
});

export default Question;
