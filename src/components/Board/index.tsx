import React, {useEffect} from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {AppDispatch, RootState} from '../../store';
import {getLeaderboards} from '../../store/leaderboard/action';

const Leaderboard = () => {
  const dispatch = useDispatch<AppDispatch>();
  const users = useSelector((state: RootState) => state.leaderboard.users);
  const step = useSelector((state: RootState) => state.user.step);

  const isRender = step === 'board';

  useEffect(() => {
    dispatch(getLeaderboards());
  }, []);
  
  if (!isRender) return null;

  if (users.length === 0)
    return <Text style={styles.container}>'loeading'</Text>;
  return (
    <View style={styles.container}>
      <Text style={styles.title}>LEADERBOARD</Text>
      <FlatList
        data={users}
        keyExtractor={item => item.id}
        renderItem={({item, index}) => (
          <View style={styles.player}>
            <Text style={styles.playerName}>{index + 1}</Text>
            <Text style={styles.playerName}>{item.username}</Text>
            <Text style={styles.playerScore}>{item.points}</Text>
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#D0E1FF',
  },
  title: {
    fontSize: 24,
    marginTop: 40,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#1E8CFB',
    marginBottom: 20,
  },
  topThree: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 20,
  },
  topPlayer: {
    alignItems: 'center',
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  points: {
    fontSize: 16,
    color: '#888',
  },
  player: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
    backgroundColor: '#FFF',
    borderRadius: 10,
    marginBottom: 10,
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 8,
    elevation: 1,
  },
  playerName: {
    fontSize: 16,
  },
  playerScore: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#E74C3C',
  },
});

export default Leaderboard;
