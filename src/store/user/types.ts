export interface UserState {
  name: string | null;
  history: Array<HistoryPoint>;
  step: 'set-user-name' | 'question' | 'board'
}

export interface HistoryPoint {
  round: number;
  point: number;
}

export enum UserActionTypes {
  SET_USER_NAME = '@@user/SET_USER_NAME',
  ADD_HISTORY_POINT = '@@user/ADD_HISTORY_POINT',
  SET_STATE = '@@user/SET_STATE',
}
