import {UserActionTypes, UserState} from './types';

const initialState: UserState = {
  name: null,
  history: [],
  step: 'set-user-name', //'board'//
};

const userReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case UserActionTypes.SET_USER_NAME:
      return {
        ...state,
        name: action.payload,
        step: 'question',
      };
    case UserActionTypes.ADD_HISTORY_POINT:
      return {
        ...state,
        step: 'board',
        history: [
          ...state.history,
          {
            point: action.payload,
            round: state.history.length + 1,
          },
        ],
      };
    case UserActionTypes.SET_STATE:
      return {
        ...state,
        step: action.payload,
      };
    default:
      return state;
  }
};

export default userReducer;
