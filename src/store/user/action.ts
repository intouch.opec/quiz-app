import {UserActionTypes, HistoryPoint} from './types';

export const setUsername = (username: string) => ({
  type: UserActionTypes.SET_USER_NAME,
  payload: username,
});

export const addHistory = (point: number) => ({
  type: UserActionTypes.ADD_HISTORY_POINT,
  payload: point,
});

export const setState = (state: 'set-user-name' | 'question' | 'board') => ({
  type: UserActionTypes.ADD_HISTORY_POINT,
  payload: state,
});
