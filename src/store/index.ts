import {
  configureStore,
  ThunkAction,
  Action,
  combineReducers,
} from '@reduxjs/toolkit';
import leaderboardReducer from './leaderboard/reducer';
import questionReducer from './question/reducer';
import userReducer from './user/reducer';
export type RootState = ReturnType<typeof store.getState>;

const rootReducer = combineReducers({
  leaderboard: leaderboardReducer,
  question: questionReducer,
  user: userReducer,
});

const store = configureStore({
  reducer: rootReducer,
  // middleware: getDefaultMiddleware =>
  //   getDefaultMiddleware().concat(require('redux-thunk').default),
  devTools: true,
});

export type AppDispatch = typeof store.dispatch;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;

export default store;
