export interface Question {
  question: string;
  options: Array<string>;
  answer: string;
}

export interface QuestionsState {
  questions: Array<Question>;
  ansers: Array<string>;
  current: number;
  point: number;
}

export enum QuestionActionTypes {
  INITAL_QUESTIONS = '@@question/INITAL_QUESTIONS',
  ADD_ANSWER = '@@question/ADD_ANSWER',
  SET_CURRENT_QUESTION_INDEX = '@@question/SET_CURRENT_QUESTION_INDEX',
  SET_QUESTIONS = '@@question/SET_QUESTIONS',
  ADD_POINT = '@@question/ADD_POINT',
}
