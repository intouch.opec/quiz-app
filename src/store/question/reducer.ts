import {QuestionActionTypes, QuestionsState} from './types';

const initialState: QuestionsState = {
  questions: [],
  ansers: [],
  current: 0,
  point: 0,
};

const questionReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case QuestionActionTypes.INITAL_QUESTIONS:
      return {
        ...state,
        ansers: [],
        questions: action.payload,
      };
    case QuestionActionTypes.SET_CURRENT_QUESTION_INDEX:
      return {
        ...state,
        current: state.current++,
      };
    case QuestionActionTypes.ADD_POINT:
      return {
        ...state,
        current: state.current + 1,
        point: state.point + action.payload,
      };
    case QuestionActionTypes.ADD_ANSWER:
      return {
        ...state,
        ansers: [...state.ansers, action.payload],
      };
    default:
      return state;
  }
};

export default questionReducer;
