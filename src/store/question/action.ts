import {QuestionActionTypes} from './types';
import questions from '../../json/questions.json';
import {map} from 'lodash';

export const setCurrentQuestionIndex = () => ({
  type: QuestionActionTypes.SET_CURRENT_QUESTION_INDEX,
});

export const getRandomNumbers = (total: number, count: number): number[] => {
  const numbers = Array.from({length: total}, (_, index) => index + 1);

  for (let i = numbers.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [numbers[i], numbers[j]] = [numbers[j], numbers[i]];
  }

  return numbers.slice(0, count);
};

export const setQuestions = () => {
  return {
    type: QuestionActionTypes.INITAL_QUESTIONS,
    payload: map(
      getRandomNumbers(questions.length, 20),
      (index: number) => questions[index],
    ),
  };
};

export const setAddAnwer = (index: number, answer: string) => ({
  type: QuestionActionTypes.ADD_ANSWER,
  payload: {index, answer},
});

export const addUserScore = (point: number) => ({
  type: QuestionActionTypes.ADD_POINT,
  payload: point,
});
