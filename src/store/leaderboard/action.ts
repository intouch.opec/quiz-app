import leaderboards from '../../json/leaderboard.json';
import {LeaderboardActionTypes} from './types';

export const setLeaderboards = ({
  username: username,
  points,
}: {
  username: string;
  points: number;
}) => {
  const payload = [
    ...leaderboards,
    {
      username,
      points,
    },
  ];
  return {
    type: LeaderboardActionTypes.SET_LEADERBOARD,
    payload: payload.sort((a, b) => b.points - a.points),
  };
};

export const getLeaderboards = () => {
  return {
    type: LeaderboardActionTypes.GET_LEADERBOARD,
  };
};
