import {LeaderboardActionTypes, LeaderboardState} from './types';

const initialState: LeaderboardState = {
  users: [],
};

const leaderboardReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case LeaderboardActionTypes.SET_LEADERBOARD:
      return {
        ...state,
        users: action.payload,
      };
    case LeaderboardActionTypes.GET_LEADERBOARD:
      return {
        ...state      };
    default:
      return state;
  }
};

export default leaderboardReducer;
