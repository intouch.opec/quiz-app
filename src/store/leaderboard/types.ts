export interface LeaderboardState {
  users: {username: string; points: number}[];
}

export enum LeaderboardActionTypes {
  SET_LEADERBOARD = '@@leaderboard/SET_LEADERBOARD',
  GET_LEADERBOARD = '@@leaderboard/GET_LEADERBOARD',
}
